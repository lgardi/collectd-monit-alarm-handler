import sys

import pytest
from mock import MagicMock, Mock, PropertyMock, patch, mock_open, call

@pytest.fixture
def collectd_monit_alarm_handler():
    collectd = MagicMock()

    # Add Collectd notification severities
    collectd.NOTIF_FAILURE = 1
    collectd.NOTIF_WARNING = 2

    teigi = MagicMock()
    rogerclient = MagicMock()
    with patch.dict('sys.modules', {'collectd': collectd, 'teigi': teigi, 'teigi.rogerclient': rogerclient}):
        import collectd_monit_alarm_handler
        yield collectd_monit_alarm_handler

def test_plugin_registration(collectd_monit_alarm_handler):
    collectd_monit_alarm_handler.collectd.register_config_assert_called_once_with(collectd_monit_alarm_handler.configure_callback)

@patch('collectd_monit_alarm_handler._create_roger_client', return_value='None')
def test_configure_defaults(_create_roger_client, collectd_monit_alarm_handler):
    collectd_monit_alarm_handler.configure_callback(Mock(children = [], values = []))

    assert collectd_monit_alarm_handler.CONFIG[collectd_monit_alarm_handler.ROGER_ALARMED_DEFAULT] == collectd_monit_alarm_handler.ROGER_ALARMED_DEFAULT_DEFAULT
    assert collectd_monit_alarm_handler.CONFIG[collectd_monit_alarm_handler.ROGER_CONF_PATH] == collectd_monit_alarm_handler.ROGER_CONF_PATH_DEFAULT
    assert collectd_monit_alarm_handler.CONFIG[collectd_monit_alarm_handler.ROGER_CACHE_FILE] == collectd_monit_alarm_handler.ROGER_CACHE_FILE_DEFAULT
    assert collectd_monit_alarm_handler.CONFIG[collectd_monit_alarm_handler.ALARMS_EXTRA_PATH] == collectd_monit_alarm_handler.ALARMS_EXTRA_PATH_DEFAULT

    assert collectd_monit_alarm_handler.CONFIG['default_targets'] == {'snow': {}, 'email': {}, 'targets': []}

    collectd_monit_alarm_handler._create_roger_client.assert_called_once_with(collectd_monit_alarm_handler.ROGER_CONF_PATH_DEFAULT)

@patch('collectd_monit_alarm_handler._create_roger_client', return_value='None')
def test_configure_custom_targets_snow(_create_roger_client, collectd_monit_alarm_handler):
    collectd_monit_alarm_handler.configure_callback(MagicMock(key = 'Module', children = (
        [MagicMock(key = 'DefaultTargets', values = ['snow'])] +
        [MagicMock(key = 'SnowFunctionalElement', values = ['MyFE'])] +
        [MagicMock(key = 'SnowServiceElement', values = ['MySE'])] +
        [MagicMock(key = 'SnowAssignmentLevel', values = [2])] +
        [MagicMock(key = 'SnowGrouping', values = [False])]
    )))

    assert collectd_monit_alarm_handler.CONFIG['default_targets']['targets'] == ['snow']

    assert collectd_monit_alarm_handler.CONFIG['default_targets']['snow']['functional_element'] == 'MyFE'
    assert collectd_monit_alarm_handler.CONFIG['default_targets']['snow']['service_element'] == 'MySE'
    assert collectd_monit_alarm_handler.CONFIG['default_targets']['snow']['assignment_level'] == 2
    assert collectd_monit_alarm_handler.CONFIG['default_targets']['snow']['grouping'] == False

@pytest.mark.parametrize("notification, expected_namespace", [
    (MagicMock(plugin='df', plugin_instance='root', type='percentage', type_instance='used'),
        ['df', 'root', 'percentage', 'used']),
    (MagicMock(plugin='cpu', type='gauge'),
        ['cpu', 'gauge'])
    ])
def test_generate_namespace(collectd_monit_alarm_handler, notification, expected_namespace):
    """ Can't be tested properly because MagicMock doesn't produce AttributeError
        when using __getattr__ to access a non existent attribute
    """
    namespace = collectd_monit_alarm_handler._generate_namespace(notification)

    assert list(filter(lambda x: isinstance(x, str), namespace)) == expected_namespace

@pytest.mark.parametrize("nplugin, ntype, expected_name", [
    ('df', 'percentage', 'df_percentage'),
    ('load', 'load_short', 'load_short'),
    ('cpu', None, 'cpu')
])
def test_generate_metric_name(collectd_monit_alarm_handler, nplugin, ntype, expected_name):
    metric_name = collectd_monit_alarm_handler._generate_metric_name(nplugin, ntype)

    assert metric_name == expected_name

@pytest.mark.parametrize("severity, expected_status", [
    (0, 'OK'),
    (1, 'FAILURE'),
    (2, 'WARNING')
])
def test_generate_status(collectd_monit_alarm_handler, severity, expected_status):    
    status = collectd_monit_alarm_handler._generate_status(severity)

    assert status == expected_status


@pytest.mark.parametrize("nplugin, nplugin_instance, expected_type", [
    ('cpu', None, 'os'),
    ('df', 'root', 'os'),
    ('df', 'var', 'app'),
    ('flume', 'essink', 'app')
])
def test_generate_alarm_type(collectd_monit_alarm_handler, nplugin, nplugin_instance, expected_type):
    alarm_type = collectd_monit_alarm_handler._generate_alarm_type(nplugin, nplugin_instance)

    assert alarm_type == expected_type


@pytest.mark.parametrize("alarm_type, host, expected_status", [
    ('hw', 'host1', True),
    ('app', 'host1', False),
    ('os', 'host2', False),
])
def test_is_roger_alarmed(collectd_monit_alarm_handler, alarm_type, host, expected_status):
    roger_mock = MagicMock()
    roger_mock.open_url = MagicMock(return_value = {'app_alarmed': False, 'hw_alarmed': True, 'os_alarmed': False})
    roger_mock.state_url = MagicMock(return_value = 'url')
    collectd_monit_alarm_handler.ROGER_CLIENT = roger_mock

    status = collectd_monit_alarm_handler._is_roger_alarmed(alarm_type, host)
    roger_mock.open_url.assert_called_once_with('url')
    roger_mock.state_url.assert_called_once_with(host)

    assert status == expected_status


@pytest.mark.parametrize("notification, expected_correlation", [
    (MagicMock(meta={'FailureMax': 12, 'WarningMax': 'nan', 'FailureMin': 'nan', 'WarningMin': 'nan', 'CurrentValue': 13}), '13.0 > 12.0'),
    (MagicMock(meta={'FailureMax': 'nan', 'WarningMax': 'nan', 'FailureMin': 14, 'WarningMin': 'nan', 'CurrentValue': 13}), '13.0 < 14.0'),
    (MagicMock(meta={'FailureMax': 12, 'WarningMax': 11, 'FailureMin': 'nan', 'WarningMin': 'nan', 'CurrentValue': 13}), '13.0 > 11.0 AND 13.0 > 12.0'),
    (MagicMock(meta={}), '')
])
def test_generate_correlation(collectd_monit_alarm_handler, notification, expected_correlation):
    correlation = collectd_monit_alarm_handler._generate_correlation(notification)
    assert correlation == expected_correlation


@pytest.mark.parametrize("notification, expected_document", [
    (MagicMock(
        host='host1', message='message', alert_name='root_full',
        meta={
          'source': 'collectd',
          'metrics': 'df_root',
          'status': 'OK',
          'alarm_type': 'app',
          'roger_alarmed': False,
          'correlation': '11.0 > 10.0',
          'collectd_namespace': 'df_root_percentage_used',
          'targets': {
              'targets': ['snow'],
              'snow': {'fename': 'fename'}
          },
          'troubleshooting': 'test'
        }
    ),
    {
      'source': 'collectd',
      'metrics': 'df_root',
      'alarm_name': 'root_full',
      'collectd_namespace': 'df_root_percentage_used',
      'entities': 'host1',
      'status': 'OK',
      'alarm_type': 'app',
      'roger_alarmed': False,
      'correlation': '11.0 > 10.0',
      'description': 'message',
      'targets': ['snow'],
      'snow': {'fename': 'fename'},
      'troubleshooting': 'test'
    }),
])
def test_to_alarm_document(collectd_monit_alarm_handler, notification, expected_document):
    document = collectd_monit_alarm_handler._to_alarm_document(notification)

    assert document == expected_document
