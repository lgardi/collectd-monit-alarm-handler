from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()

version = '1.0.14'

install_requires = [
    'requests', 'pyyaml', 'ConfigParser' 
]


setup(name='collectd_monit_alarm_handler',
    version=version,
    description="Collectd Plugin to send alarms to the monitoring infrastructure",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd alarms notification monitoring',
    url = 'https://gitlab.cern.ch/monitoring/collectd-monit-alarm-handler',
    author='Borja Garrido Bear',
    author_email='borja.garrido.bear@cern.ch',
    maintainer='CERN IT Monitoring',
    maintainer_email='monit-support@cern.ch',
    license='Apache II',
    packages=find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)
