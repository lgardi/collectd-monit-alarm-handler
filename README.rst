Monit alarm handler Collectd Plugin
============================

Configuration
-------------

Example::

    <Plugin "python">

      Import "collectd_monit_alarm_handler"

      <Module "collectd_monit_alarm_handler">
          RogerAlarmedDefault false
          RogerConfPath "/etc/roger.conf"
          RogerCacheFile "/etc/roger/current.yaml"
          ConfigFilesPath "/opt/monit/collectd/alarms/"
          TargetsBase "/opt/monit/collectd/alarms/default_targets.json"
          AlarmTypes "/opt/monit/collectd/alarms/alarm_types.json"
      </Module>

    </Plugin>


* ``RogerAlarmedDefault``: (Optional) Fallback value for alarms in case the script can't contact roger. Default: False.
* ``RogerConfPath``: (Optional) Path for the roger configuration file. Default: "/etc/roger.conf"
* ``RogerCacheFile``: (Optional) Path for the roger current file inside the host. Default: "/etc/roger/current.yaml"
* ``ConfigFilesPath``: (Optional) Path to find the configuration files for the script. Default: "/opt/monit/collectd/alarms/".
* ``TargetsBase``: (Optional) Path to find the default targets file for the Collectd alarms. Default: "/opt/monit/collectd/alarms/default_targets.json".
* ``AlarmTypes``: (Optional) Path to find the alarm types configuration. Default: "/opt/monit/collectd/alarms/alarm_types.conf".

Alarms
-------

The alarms generated by the plugin have the next schema::


    {
        'status': 'FAILURE',
        'targets': [],
        'entities': 'monit-amqsource-dev-beb4228aa6.cern.ch',
        'alarm_type': 'os',
        'alarm_name': 'df_root_percent_bytes_used_value',
        'snow': {
                'functional_element': 'Monitoring',
                'assignment_level': 3,
                'service_element': 'default',
                'grouping': True,
                'functional_category': '',
                'auto_closing': True
        },
        'email': {
                'to': ['someone@cern.ch']
        },
        'troubleshooting': u '',
        'metrics': 'df_percent_bytes',
        'source': 'collectd',
        'correlation': '29.8219718933 > 1.0',
        'message': 'Host monit-amqsource-dev-beb4228aa6.cern.ch, plugin df (instance root) type percent_bytes (instance used): Data source "value" is currently 29.821972. That is above the failure threshold of 1.000000.',
        'roger_alarmed': True
    }

Namespaces
-----------

As Collectd alarms can be defined with different level of granularity (plugin, plugin and type, plugin and plugin instance, etc.) this plugin does a merge of all the configured options for a givel alarm.

e.g: Let's say we hace the alarm df_root_percent and we have defined the next things::

    # df.yaml
    targets:
      enabled: ['snow']
      snow:
        disabled: false
        functional_element: 'fe1'
    troubleshooting: 'something'
    
    ---
    
    # df_root.yaml
    targets:
      enabled: []
      snow:
        disabled: true
        service_element: 'se1'
    troubleshooting: 'https://my.trouble-shooting-link.com'
    
    ---
    
    # df_root_percent.yaml
    targets:
      enabled: ['snow','email']
      snow:
        disabled: false
        functional_element: 'fe2'
      email:
        disabled: false
        to: ['someone@cern.ch']
    troubleshooting: 'https://my.trouble-shooting-link.com'


The behaviour in this case is:

- alarms for df plugin that are not df_root will have the configuration from df.yaml

- alarms for df_root plugin that are not df_root_percent will have the configuration merged from df.yaml and df_root.yaml

  * targets: [] (Since targets always takes into account the disabled flag on the most specific file)

  * functional_element: 'fe1'

  * service_element: 'se1'

* alarms for df_root_percent will have the configuration merged from the three files

  * targets: ['snow','email']

  * functional_element: 'fe2'

  * service_element: 'se1'

  * to: ['someone@cern.ch']
