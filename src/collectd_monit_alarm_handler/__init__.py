import collectd

import copy
import requests
import yaml
import time
import os
try:
    import simplejson as json
except ImportError:
    import json
from math import isnan
from teigi.rogerclient import RogerClient


ROGER_ALARMED_DEFAULT = 'roger_alarmed_default'
ROGER_CONF_PATH = 'roger_conf_path'
ROGER_CACHE_FILE = 'roger_cache_file'
PUPPET_CACHE_FILE = 'puppet_cache_file'
ALARMS_EXTRA_PATH = 'alarms_extra_path'

ROGER_ALARMED_DEFAULT_DEFAULT = False
ROGER_CONF_PATH_DEFAULT = '/etc/roger.conf'
ROGER_CACHE_FILE_DEFAULT = '/etc/roger/current.yaml'
PUPPET_CACHE_FILE_DEFAULT = '/etc/roger/puppet.yaml'
ALARMS_EXTRA_PATH_DEFAULT = '/etc/collect.d/alarms'

CONFIG = {
    ROGER_ALARMED_DEFAULT: ROGER_ALARMED_DEFAULT_DEFAULT,
    ROGER_CONF_PATH: ROGER_CONF_PATH_DEFAULT,
    ROGER_CACHE_FILE: ROGER_CACHE_FILE_DEFAULT,
    PUPPET_CACHE_FILE: PUPPET_CACHE_FILE_DEFAULT,
    ALARMS_EXTRA_PATH: ALARMS_EXTRA_PATH_DEFAULT
}

ROGER_CLIENT = None
NAMESPACE_FIELDS = ['plugin', 'plugin_instance', 'type', 'type_instance', 'DataSource']
COLLECTD_THRESHOLD = [('WarningMin', '<'), ('FailureMin', '<'), ('WarningMax', '>'), ('FailureMax', '>')]
ALARM_TYPES = {
  "os": ["cpu", "df_root", "disk", "load", "memory", "tail_base", "swap", "uptime", "vmem"],
  "hw": ["mdstat", "sasarray", "megaraidsas", "blockdevice_drivers", "adaptec", "smart_tests"]
}

NOTIFICATION_SEND_ATTEMPTS = 6
NOTIFICATION_SEND_ATTEMPTS_INTERVAL_SEC = 5

def configure_callback(config):
    global CONFIG

    cfg = _map_collectd_config(config)

    _get_optional_configuration(CONFIG, cfg, ROGER_ALARMED_DEFAULT, 'Module.RogerAlarmedDefault')
    _get_optional_configuration(CONFIG, cfg, ROGER_CONF_PATH, 'Module.RogerConfPath')
    _get_optional_configuration(CONFIG, cfg, ROGER_CACHE_FILE, 'Module.RogerCacheFile')
    _get_optional_configuration(CONFIG, cfg, ALARMS_EXTRA_PATH, 'Module.AlarmsExtraPath')

    try:
        _create_roger_client(CONFIG[ROGER_CONF_PATH])
    except Exception as e:
        collectd.warning("Roger client couldn't be configured for alarm handler plugin {0}".format(str(e)))

    _load_default_targets(CONFIG, cfg)

    collectd.info("Collectd alarm handler loaded with configuration : {0!r}".format(CONFIG))
    collectd.register_notification(notification_callback)


def notification_callback(notification):
    namespace = _generate_namespace(notification)

    notification.meta['collectd_namespace'] = "_".join(namespace)
    notification.meta['source'] = 'collectd'
    notification.meta['metrics'] = _generate_metric_name(notification.plugin, notification.type)
    notification.meta['status'] = _generate_status(notification.severity)
    notification.meta['alarm_type'] = _generate_alarm_type(notification.plugin, notification.plugin_instance)
    notification.meta['roger_alarmed'] = _is_roger_alarmed(notification.meta['alarm_type'], notification.host)
    notification.meta['correlation'] = _generate_correlation(notification)

    custom_targets, troubleshooting = _generate_custom_targets(namespace)
    notification.meta['targets'] = custom_targets
    notification.meta['troubleshooting'] = troubleshooting

    _send_and_check(_to_alarm_document(notification))


def _create_roger_client(config_file):
    """ Creates the roger client object inside the ROGER_CLIENT
        global variable

    Args:
        config_file (string): Path to the file where Roger config is stored
    """
    from ConfigParser import SafeConfigParser
    global ROGER_CLIENT
    roger_conf = SafeConfigParser()
    roger_conf.read(config_file)

    ROGER_CLIENT = RogerClient(
        roger_conf.get("client", "server"),
        roger_conf.getint("client", "sslport"),
        use_kerberos=False,
        deref_alias=roger_conf.getboolean("client", "dereference_alias")
    )


def _get_optional_configuration(config, cfg, config_key, module_key):
    """ Loads configuration producer by _map_collectd_config
        into the config dictionary

    Args:
        config (dictionay): dictionary to set the key into
        cfg (dictionary): _map_collectd_config returned object
        config_key (string): Name of the key in the CONFIG object
        module_key (string): Name of the key in the cfg object

    Returns:
        string: the arg if it's only one
    """
    if module_key in cfg:
        config[config_key] = cfg[module_key][0]


def _generate_alarm_type(nplugin, nplugin_instance):
    """ Generates the alarm type of a notification based on its
        plugin and plugin_instance

    Args:
        nplugin (string): Plugin of the notification
        nplugin_instance (string): Plugin instance of the notification

    Returns:
        string: One of "app", "hw" or "os"
    """
    alarm_type_name = "%s_%s" % (nplugin, nplugin_instance) if nplugin_instance is not None else nplugin
    alarm_type = "app"
    if alarm_type_name in ALARM_TYPES['os'] or nplugin in ALARM_TYPES['os']:
        alarm_type = "os"
    elif alarm_type_name in ALARM_TYPES['hw'] or nplugin in ALARM_TYPES['hw']:
        alarm_type = "hw"

    return alarm_type


def _generate_correlation(notification):
    """ Generates the correlation for the notification using the
        global COLLECTD_THRESHOLD list

    Args:
        notification: Collectd notification object

    Returns:
        string: Correlation of the notification
    """
    correlation = ""
    for field, side in COLLECTD_THRESHOLD:
        try:
            value = float(notification.meta[field])
            if not isnan(value):
                if correlation:
                    correlation += " AND {0} {1} {2}".format(float(notification.meta['CurrentValue']), side, value)
                else:
                    correlation = "{0} {1} {2}".format(float(notification.meta['CurrentValue']), side, value)
        except (KeyError, TypeError, ValueError):
            pass

    return correlation


def _generate_custom_targets(namespace_fields):
    """ Generates the targets for the alarm based on the namespace
        It loads first the default ones from the global CONFIG dictionary
        and updates them with the custom ones

    Args:
        namespace_fields (list): produced by _generate_namespace

    Returns:
        dictionary: with the targets for the notification 
    """
    target_fields = copy.deepcopy(CONFIG['default_targets'])
    troubleshooting = None

    namespaces = ["_".join(namespace_fields[0:x]) for x in range(1, len(namespace_fields) + 1)]
    for namespace in namespaces:
        try:
            alarm_file_path = CONFIG[ALARMS_EXTRA_PATH] + "/" + namespace + ".yaml"
            if os.path.isfile(alarm_file_path):
                with open(alarm_file_path) as extra_alarm_file:
                   yaml_file = yaml.load(extra_alarm_file)
                   if ("troubleshooting" in yaml_file):
                       troubleshooting = yaml_file['troubleshooting']
                   custom_targets = yaml_file['targets']
                   target_fields['targets'] = list(set(set(custom_targets['enabled']) | set(target_fields['targets'])))
                   for target in custom_targets:
                       if target != 'enabled':
                           target_fields[target].update(custom_targets[target])
                           if target_fields[target]['disabled'] and target in target_fields['targets']:
                               target_fields['targets'].remove(target)
                           target_fields[target].pop('disabled', None)
        except Exception as ex:
            collectd.warning("Error while generating the custom targets for the alarm: %s" % ex)

    return target_fields, troubleshooting


def _generate_metric_name(nplugin, ntype):
    """ Generates the metric name based in the plugin and the type

    Args:
        nplugin (string): Notification plugin
        ntype (string): Notification type

    Returns:
        string: metric name of the notification
    """
    metric_name = "%s_%s" % (nplugin, ntype)
    if ntype is None:
        metric_name = nplugin
    elif ntype.startswith(nplugin):
        metric_name = ntype

    return metric_name


def _generate_namespace(notification):
    """ Generates the namespace of a notification based on the
        configured namespace_fields in the global CONFIG dictionary

    Args:
        notification: Collectd notification object

    Returns:
        list: List with the namespace values sorted inside
    """
    namespace = []
    for field in NAMESPACE_FIELDS:
        try:
            if getattr(notification, field) is not None:
                namespace.append(getattr(notification, field))
        except AttributeError:
            if field in notification.meta:
                namespace.append(notification.meta[field])

    return list(filter(None, namespace))


def _generate_status(severity):
    """ Generates the status value based on the notification severity

    Args:
        severity (int): Collectd severity value

    Returns:
        string: "OK", "WARNING" or "FAILURE"
    """
    status = "OK"
    if severity == collectd.NOTIF_FAILURE:
        status = "FAILURE"
    elif severity == collectd.NOTIF_WARNING:
        status = "WARNING"

    return status


def _is_roger_alarmed(alarm_type, host):
    """ Checks if roger is alarms against the Roger server
        and the local configuration if it fails

    Args:
        alarm_type (string): Type of the alarm produced by _generate_alarm_type
        host (string): FQDN of the host the notification is reporting for

    Returns:
        bool: True if roger is alarmed, False otherwise
    """
    roger_alarm = "%s_alarmed" % (alarm_type)
    alarmed = CONFIG[ROGER_ALARMED_DEFAULT]
    try:
        with open(CONFIG[ROGER_CACHE_FILE], 'r') as roger_cache_file:
            with open(CONFIG[PUPPET_CACHE_FILE], 'r') as puppet_cache_file:
                roger_yaml = yaml.safe_load(roger_cache_file)
                puppet_yaml = yaml.safe_load(puppet_cache_file)

                most_recent_yaml = roger_yaml if roger_yaml["update_time"] > puppet_yaml["update_time"] else puppet_yaml
                alarmed = most_recent_yaml[roger_alarm]
    except Exception as e:
        collectd.warning("Exception while trying to look at the cache files: {0}".format(str(e)))
        try:
            alarmed = ROGER_CLIENT.open_url(ROGER_CLIENT.state_url(host))[roger_alarm]
        except Exception:
            pass

    return alarmed


def _load_default_targets(config, cfg):
    """ Loads the default targets dictionary configured in the system
    
    Args:
        config (dictionary): Dictionary to set the keys into
        cfg (dictionary): Dictionary to get the keys from

    """
    default_targets = {}
    default_targets['targets'] = []

    if 'Module.DefaultTargets' in cfg:
        default_targets['targets'] = cfg['Module.DefaultTargets']

    snow = {}
    _get_optional_configuration(snow, cfg, 'functional_element',
                                'Module.SnowFunctionalElement')
    _get_optional_configuration(snow, cfg, 'service_element',
                                'Module.SnowServiceElement')
    _get_optional_configuration(snow, cfg, 'assignment_level',
                                'Module.SnowAssignmentLevel')
    _get_optional_configuration(snow, cfg, 'grouping',
                                'Module.SnowGrouping')
    _get_optional_configuration(snow, cfg, 'functional_category',
                                'Module.SnowFunctionalCategory')
    _get_optional_configuration(snow, cfg, 'auto_closing',
                                'Module.SnowAutoClosing')

    email = {}

    default_targets['snow'] = snow
    default_targets['email'] = email

    config['default_targets'] = default_targets


def _map_collectd_config(config):
    """ Turns Collectd config into a dictionary

    Args:
        config: Collectd configuration object

    Returns:
        dictionary: Each key being a concatenation of each key and his parent
    """
    cfgmap = {}
    child_stack = [ ("", config), ]
    while len(child_stack) > 0:
        (scope, cfg) = child_stack.pop()
        if len(scope) > 0:
            prefix = "{0}.{1}".format(scope, cfg.key)
        else:
            prefix = cfg.key
        for child in cfg.children:
            child_stack.append((prefix, child))
        if len(cfg.values) > 0:
            cfgmap[prefix] = cfg.values

    return cfgmap


def _send_and_check(document):
    """ Sends and alarm document and checks the result.
    Retries as per the NOTIFICATION_SEND_ATTEMPTS and NOTIFICATION_SEND_ATTEMPTS_INTERVAL_SEC configuration if the
    notification has not been sent correctly

    Args:
        document (dictionary): result of invoking _to_alarm_document
    """
    for attempt in range(1, NOTIFICATION_SEND_ATTEMPTS + 1):
        try:
            response = requests.post('http://127.0.0.1:12002/', data=json.dumps(document), headers={ "Content-Type": "application/json; charset=UTF-8"})
            response.raise_for_status()
        except Exception as e:
            attempts_left = NOTIFICATION_SEND_ATTEMPTS - attempt
            collectd.info("Failed attempt to send notification {1}, Error: {2}. The process will be retried after {3} seconds "
                          "with {4} more attempts left.".format(attempt, document, e, NOTIFICATION_SEND_ATTEMPTS_INTERVAL_SEC, attempts_left))
            time.sleep(NOTIFICATION_SEND_ATTEMPTS_INTERVAL_SEC)
        else:
            break
    else:
        collectd.warning("Failed all attempts of sending notification {0}, Error: {1}".format(document, e))

def _to_alarm_document(notification):
    """ Transforms a Collectd notification object into a
        monitoring compilant alarm dictionary

    Args:
      notification: Collectd notification object

    Returns:
      dictionary: representation of an alarm document in a dictionary
    """
    monit_alarms_document = {}
    monit_alarms_document["source"] = notification.meta['source']
    monit_alarms_document["metrics"] = notification.meta['metrics']

    alert_name = notification.meta['collectd_namespace']
    try:
        if notification.alert_name:
            alert_name = notification.alert_name
    except AttributeError:
        collectd.warning("No alert name attribute in the notification object, you might be running an old Collectd version")

    monit_alarms_document["alarm_name"] = alert_name
    monit_alarms_document["collectd_namespace"] = notification.meta['collectd_namespace']
    monit_alarms_document["entities"] = notification.host
    monit_alarms_document["status"] = notification.meta['status']
    monit_alarms_document["alarm_type"] = notification.meta['alarm_type']
    monit_alarms_document["roger_alarmed"] = notification.meta['roger_alarmed']
    monit_alarms_document["correlation"] = notification.meta['correlation']
    monit_alarms_document["troubleshooting"] = notification.meta['troubleshooting']
    monit_alarms_document["description"] = notification.message

    monit_alarms_document.update(notification.meta['targets'])

    return monit_alarms_document


collectd.register_config(configure_callback)
