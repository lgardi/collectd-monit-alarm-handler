# Created by pyp2rpm-3.3.2
%global pypi_name collectd_monit_alarm_handler

Name:           python-%{pypi_name}
Version:        1.0.14
Release:        1%{?dist}
Summary:        Collectd Plugin to send alarms based on Collectd notifications

License:        Apache II
URL:            https://gitlab.cern.ch/monitoring/collectd-monit-alarm-handler
Source0:        https://gitlab.cern.ch/monitoring/collectd-monit-alarm-handler/-/archive/%{version}/collectd-monit-alarm-handler-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

%description


%package -n     python2-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{pypi_name}}
Requires: python-requests

%description -n python2-%{pypi_name}



%prep
%autosetup -n collectd-monit-alarm-handler-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}

%post -n python2-%{pypi_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n python2-%{pypi_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files -n python2-%{pypi_name}
%doc README.rst LICENSE
%{python_sitelib}/%{pypi_name}
%{python_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Mon May 27 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 1.0.14-1
- [MONIT-2087] Handle better no occurrence of alert name in the alerts

* Wed Mar 27 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 1.0.13-2
- [MONIT-1977] Add support for alert name field and rename message to description

* Mon Mar 18 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 1.0.12-1
- [NOTICKET] Handle better non existing file while checking for custom targets

* Tue Mar 12 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.11-1
- [MONIT-1949] Move troubleshooting out of snow, integrate email-to

* Mon Mar 04 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 1.0.10-1
- [MONIT-1937] Fix targets computation

* Fri Feb 20 2019 Nikolay Tsvetkov <n.tsvetkov@cern.ch> - 1.0.9-2
- Retry sending notifications to the endpoint in case of failure. Performs up to 6 attempts with 5 seconds interval

* Fri Feb 08 2019 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 1.0.8-1
- Allow automatic closing of snow tickets from OK alarms

* Tue Feb 05 2019 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 1.0.7-1
- Add support to specify FE categories in SNOW tickets

* Mon Jan 07 2019 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.6-2
- Rename smart plugin to smart_tests

* Fri Nov 23 2018 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.5-1
- Make sure alarms configuration is not used among different alarms

* Fri Oct 26 2018 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 1.0.4-1
- Both Puppet and megabus cache files considered when querying the roger state; the most recent one is used

* Thu Oct 25 2018 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 1.0.3-1
- Update collectd alarm handler to query the roger cache first

* Thu Oct 4 2018 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.2-1
- Add hardware plugins clasification

* Fri Sep 21 2018 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.1-1
- Fixes for Python 2.6 and namespace generation

* Thu Aug 16 2018 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.0-1
- Initial package.
